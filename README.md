# Node/Express Authentication Best Practices

Given the unexpected popularity of [a Medium post]() I wrote, this repository
will serve as a living document for best practices on Node/Express authentication.
I will update this as appropriate as I have free time to build this out.

If you are interested in writing parts of this document, please start issues
so we can talk about it. It makes sense to me to split it into:

* Difficulty Levels: Beginner (for copypasta), Advanced (for devs working on systems)
* Authentication only, not Authorization
* Best practices with existing third-party modules

This is all just a brain dump for now. More to come.
